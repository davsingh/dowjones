## Setup Instructions

```sh
$ npm install
$ npm run start
```

## Additional (Optional)

Setup xclap for additional options e.g. hmr etc

```sh
$ npm install -g xclap-cli
```
 
To view all additional xclap options cd into dir

```sh
$ clap
```

```sh
$ clap dev
```

```sh
$ clap hot
```