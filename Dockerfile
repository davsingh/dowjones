FROM ubuntu:latest

MAINTAINER Dav Singh <davinder.cheema@live.co.uk>

FROM ubuntu:latest

RUN apt-get -yq update && apt-get -yq install \
    curl \
    ssh \
    python \
    wget \
    gnupg

RUN curl -sL https://deb.nodesource.com/setup_9.x | bash -

RUN apt-get -yq update && apt-get -yq install \
	build-essential \
	nodejs

RUN npm install -g node-gyp

ENV DIR /app

RUN mkdir -p $DIR

WORKDIR $DIR

COPY . $DIR

RUN node -v

RUN npm -v

RUN npm install -g xclap-cli

RUN npm install

RUN npm run build

RUN npm run prod

