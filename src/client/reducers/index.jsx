import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import app from "./app";
import articles from "./articles";
import tag from "./tag";
import search from "./search";
import section from "./section";

export default combineReducers({
  form,
  app,
  articles,
  tag,
  search,
  section
});
