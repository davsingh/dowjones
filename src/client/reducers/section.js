const initialState = {
  articles: [],
  isLoading: false,
  query: []
};

const section = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SECTION_SET":
      return {
        ...state,
        ...payload
      };

    default:
      return state;
  }
};

export default section;
