const initialState = {
  articles: [],
  isLoading: false,
  query: []
};

const search = (state = initialState, { type, payload }) => {
  switch (type) {
    case "SEARCH_SET":
      return {
        ...state,
        ...payload
      };

    default:
      return state;
  }
};

export default search;
