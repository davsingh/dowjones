const initialState = {
  articles: [],
  isLoading: false
};

const articles = (state = initialState, { type, payload }) => {
  switch (type) {
    case "ARTICLE_SET":
      return {
        ...state,
        ...payload
      };

    default:
      return state;
  }
};

export default articles;
