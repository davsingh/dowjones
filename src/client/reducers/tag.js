const initialState = {
  articles: [],
  isLoading: false
};

const tag = (state = initialState, { type, payload }) => {
  switch (type) {
    case "TAG_SET":
      return {
        ...state,
        ...payload
      };

    default:
      return state;
  }
};

export default tag;
