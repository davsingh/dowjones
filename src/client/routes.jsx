import App from "./containers/app";
import Tag from "./containers/tag";
import Section from "./containers/section";
import SearchResult from "./containers/search-result";
import Layout from "./components/layout";
import NotFound from "./components/not-found";
import { withRouter } from "react-router-dom";


/**
 * https://github.com/ReactTraining/react-router/issues/5127
 * See RFC RFC: Relative Links and Routes #5127
 * @type {*[]}
 */
const routes = [
  {
    path: "/",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/",
        component: App
      }
    ]
  },
  {
    path: "/tag/:tag",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/tag/:tag",
        component: Tag
      }
    ]
  },
  {
    path: "/search",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/search",
        component: SearchResult
      }
    ]
  },
  {
    path: "/news",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/news",
        component: Section
      }
    ]
  },
  {
    path: "/views",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/views",
        component: Section
      }
    ]
  },
  {
    path: "/people",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/people",
        component: Section
      }
    ]
  },
  {
    path: "/brexit",
    component: withRouter(Layout),
    init: "./init-top",
    exact: true,
    routes: [
      {
        path: "/brexit",
        component: Section
      }
    ]
  },
  {
    path: "*",
    component: withRouter(Layout),
    init: "./init-top",
    routes: [
      {
        path: "*",
        component: NotFound
      }
    ]
  }
];

export { routes };
