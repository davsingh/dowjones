import React, {Component, Fragment} from "react";

class Loading extends Component {
  render() {
    return (
      <Fragment>
        <div className="column is-one-third">
          <div className="card animated-load">
            <div className="card-image animated-load">
              <div className="loading-card">&nbsp;</div>
            </div>
            <div className="card-content animated-load">
              <div className="media animated-load">
                <div className="media-content animated-load">
                  <p className="title is-4 animated-load">&nbsp;</p>
                  <p className="subtitle is-6 animated-load">&nbsp;</p>
                </div>
              </div>
              <div className="content animated-load">
                <p>&nbsp;</p>
              </div>
            </div>
          </div>
        </div>
        <div className="column is-one-third">
          <div className="card animated-load">
            <div className="card-image animated-load">
              <div className="loading-card">&nbsp;</div>
            </div>
            <div className="card-content animated-load">
              <div className="media animated-load">
                <div className="media-content animated-load">
                  <p className="title is-4 animated-load">&nbsp;</p>
                  <p className="subtitle is-6 animated-load">&nbsp;</p>
                </div>
              </div>
              <div className="content animated-load">
                <p>&nbsp;</p>
              </div>
            </div>
          </div>
        </div>
        <div className="column is-one-third">
          <div className="card animated-load">
            <div className="card-image animated-load">
              <div className="loading-card">&nbsp;</div>
            </div>
            <div className="card-content animated-load">
              <div className="media animated-load">
                <div className="media-content animated-load">
                  <p className="title is-4 animated-load">&nbsp;</p>
                  <p className="subtitle is-6 animated-load">&nbsp;</p>
                </div>
              </div>
              <div className="content animated-load">
                <p>&nbsp;</p>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Loading;
