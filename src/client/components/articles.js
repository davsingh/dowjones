import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Loading from "./loading";

class Articles extends Component {

  static propTypes = {
    articles: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired
  };

  render() {
    const {
      articles, isLoading
    } = this.props;

    if (isLoading) {
      return (<Loading/>);
    }

    return (
      <Fragment>
        {articles && articles.map(article => {
          const {
            id,
            headline,
            summary,
            last_update_date: lastUpdate,
            tags,
            article_url: articleUrl
          } = article;

          return (
            <div key={id} className="column is-one-third">
            <div className="card">
              <div className="card-image">
                <figure className="image is-4by3">
                  { article.assets !== null ? <img src={article.assets[0].url} alt={article.assets[0].text} /> : <img src="https://bulma.io/images/placeholders/1280x960.png" alt="" />}
                </figure>
              </div>
              <div className="card-content">
                <div className="media">
                  <div className="media-content">
                    <p className="title is-4">{ headline }</p>
                    <p className="subtitle is-6 is-hidden-mobile">{lastUpdate}</p>
                  </div>
                </div>

                <div className="content">
                  <p>{ summary } <a href={articleUrl}>Read more</a></p>

                  <div className="tags">
                  {tags && tags.map(tag => {
                    return (<span key={tag} className="tag is-dark">
                      <Link className="has-text-white" to={`/tag/${tag}`}>{`#${tag}`}</Link>
                    </span>);
                  })}
                  </div>
                </div>
              </div>
            </div>
            </div>
          );
        })}

      </Fragment>


  );
  }
}

export default Articles;
