import React, { Component } from "react";
import PropTypes from "prop-types";
import { renderRoutes } from "react-router-config";
import { Link } from "react-router-dom";
import Search from "./../containers/search";


class Layout extends Component {

  static propTypes = {
    route: PropTypes.object.isRequired,
    children: PropTypes.object,
    history: PropTypes.object
  };

  static defaultPropTypes = {
    children: ""
  };

  render() {
    const {
      children,
      route,
      history
    } = this.props;

    return (
      <div className="columns" style={{margin: 0}}>
        <div className="column is-narrow is-one-fifth sidebar">
          <h3 className="title is-4 logo">
            <img src="https://assets.fnlondon.com/fnlondon/fn-logo.svg"/>
          </h3>
          <aside className="menu">
            <ul className="menu-list">
              <li>
                <Link to="/">Latest</Link>
              </li>
              <li>
                <Link to="/news">News</Link>
              </li>
              <li>
                <Link to="/views">View</Link>
              </li>
              <li>
                <Link to="/people">People</Link>
              </li>
              <li className="is-hidden-mobile">
                <Link to="/brexit">Brexit</Link>
              </li>
              <li className="is-hidden-mobile">
                &nbsp;
              </li>
              <li className="is-hidden-mobile">
                &nbsp;
              </li>
              <li className="is-hidden-mobile">
                <Search
                  history={history}
                />
              </li>
            </ul>
          </aside>
        </div>
        <section>
          <div className="column is-four-fifths top">
            {renderRoutes(route.routes)}
            {children}
          </div>
        </section>
      </div>
    );
  }
}

export default Layout;
