import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Articles from "./articles";

class Section extends Component {

  static propTypes = {
    articles: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired
  };

  render() {
    const {
      articles,
      isLoading
    } = this.props;

    return (
      <Fragment>
        <div className="columns is-multiline">
          <Articles
            articles={articles}
            isLoading={isLoading}
          />
        </div>
      </Fragment>
    );
  }
}

export default Section;
