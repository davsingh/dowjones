import React, { Component, Fragment } from "react";
import { Field, reduxForm } from "redux-form";
import PropTypes from "prop-types";
import { renderField } from "../helpers/forms";

class Search extends Component {

  static propTypes = {
    token: PropTypes.string.isRequired,
    product: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    onFetchSearchIfNeeded: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit({ search }) {
    const {
      onFetchSearchIfNeeded,
      token,
      product,
      user,
      history
    } = this.props;

    onFetchSearchIfNeeded({ token, product, user, search});
    history.push("/search");
  }

  render() {

    const {
      handleSubmit
    } = this.props;

    return (
      <Fragment>
          <form onSubmit={handleSubmit(this.handleSubmit)}>
            <Field
              name="search"
              component={renderField}
              type="text"
              placeholder="Search"
              label="Search"
            />
          </form>
      </Fragment>
    );
  }
}

Search = reduxForm({ //eslint-disable-line no-class-assign
  form: "searchForm"
})(Search);


export default Search;
