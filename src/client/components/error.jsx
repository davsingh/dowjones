import React, { Component } from "react";
import PropTypes from "prop-types";

class Error extends Component {

  static propTypes = {
    children: PropTypes.object.isRequired
  };

  state = {
    hasError: false
  };

  componentDidCatch() {
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong.</h1>;
    }
    return this.props.children;
  }
}

export default Error;

