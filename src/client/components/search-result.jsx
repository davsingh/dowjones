import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Articles from "./articles";

class SearchResult extends Component {

  static propTypes = {
    articles: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired
  };

  render() {
    const {
      articles,
      isLoading,
      name
    } = this.props;

    return (
      <Fragment>
        <h1 className="title">Search Results for { name }</h1>
        <div className="columns is-multiline">
          <Articles
            articles={articles}
            isLoading={isLoading}
          />
        </div>
      </Fragment>
    );
  }
}

export default SearchResult;
