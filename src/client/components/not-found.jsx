import React, { Component, Fragment } from "react";

class NotFound extends Component {
  render() {
    return (
      <Fragment>
        <p>Not Found</p>
      </Fragment>
    );
  }
}

export default NotFound;
