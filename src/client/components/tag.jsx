import React, {Component} from "react";
import PropTypes from "prop-types";
import Articles from "./articles";

class Tag extends Component {

  static propTypes = {
    articles: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired,
    match: PropTypes.object.isRequired
  };

  render() {
    const {
      articles,
      isLoading,
      match: {
        params: {
          tag
        }
      }
    } = this.props;

    return (
      <div>
        <h1 className="title">Tag Search Result for {tag}</h1>
        <div className="columns is-multiline">
          <Articles articles={articles} isLoading={isLoading}/>
        </div>
      </div>
    );
  }
}

export default Tag;
