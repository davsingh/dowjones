import React from "react";
import PropTypes from "prop-types";

export const renderField = (
  { input, label, type, meta: { touched, error, pristine, submitting }}) => (
  <div className="field has-addons">
    <div className="control">
      <input {...input} placeholder={label} type={type} className="input"/>
      {touched && error && <span>{error}</span>}
    </div>
    <div className="control">
      <button type="submit" className="button" disabled={pristine || submitting}>
        Submit
      </button>
    </div>
  </div>
);

renderField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired
};
