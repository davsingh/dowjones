import axios from "axios";

/**
 * Receive Real Time
 * @param  {object}       payload         API Response from server
 * @returns {{type: string, payload: *}}  Object
 */
const receiveRealTime = payload => ({
  type: "ARTICLE_SET",
  payload
});

/**
 * Fetch RealTime data from API
 * @param {string}      token     Token
 * @param {string}      user      User
 * @param {string}      product   Product
 * @returns {Function}  Function
 */
const fetchRealTime = ({token, user, product}) =>
  async (dispatch) => { //eslint-disable-line consistent-return
  try {
    dispatch(receiveRealTime({
      isLoading: true
    }));

    const response = await axios({
      url: "/api/realtime",
      method: "post",
      data: {
        token,
        user,
        product
      }
    });
    const { data } = response;
    return dispatch(receiveRealTime(data));
  } catch (e) {
    //@todo error handling, This can also be offloaded to the server side.
  } finally {
    dispatch(receiveRealTime({
      isLoading: false
    }));
  }
};

/**
 * Should Fetch RealTime, Check store first before making API call.
 * @param   {array}         articles      Articles list returned from API
 * @param   {boolean}       isLoading     Is request happening
 * @returns {boolean}       Boolean
 */
const shouldFetchRealTime = ({articles: { articles, isLoading }}) => {
  return !isLoading && articles.length === 0;
};

/**
 * I Could just as easily use getState, instead of passing state params in.
 */
// export const getRealTime = () => async (dispatch, getState) => {
//   const {
//     app: {
//       token,
//       product,
//       user
//     }
//   } = getState();
// }
/**
 * Fetch RealTime If Needed
 * @param {string}      token     Token
 * @param {string}      product   Product
 * @param {string}      user      User
 * @returns {Function}  Function
 */
export const fetchRealTimeIfNeeded = ({ token, product, user}) =>
  (dispatch, getState) => { // eslint-disable-line consistent-return
      if (shouldFetchRealTime(getState())) {
        return dispatch(fetchRealTime({ token, product, user}));
      }
};
