import axios from "axios/index";

const receiveSearch = (payload) => ({
  type: "SEARCH_SET",
  payload
});

const fetchSearch = ({ token, product, user, search}) =>
  async (dispatch) => { //eslint-disable-line consistent-return
  try {
    dispatch(receiveSearch({
      isLoading: true
    }));

    const response = await axios({
      url: "/api/search",
      method: "post",
      data: {
        token,
        user,
        product,
        search
      }
    });
    const { data } = response;
    return dispatch(receiveSearch(data));
  } catch (e) {
    //@todo error handling, This can also be offloaded to the server side.
  } finally {
    dispatch(receiveSearch({
      isLoading: false
    }));
  }
};

export const fetchSearchIfNeeded = ({ token, product, user, search}) => dispatch => {
  return dispatch(fetchSearch({ token, product, user, search}));
};

