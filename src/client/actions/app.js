import axios from "axios";


/**
 * Receive Credentials
 * @param   {object}        payload       Payload response from API call
 * @returns {
 *      {type: string,
 *       payload: {
 *          token: string,
 *          expires: string,
 *          product: string,
 *          user: string
 *          }}}  dispatch event
 */
const receiveCredentials = (payload) => ({
  type: "APP_SET",
  payload: {
    token: payload["api-token"],
    expires: payload["expires-in"],
    product: payload["api-product"],
    user: payload["api-user"]
  }
});

/**
 * Fetch Credentials
 * @returns {Function}    Function
 */
const fetchCredentials = () => async (dispatch) => { //eslint-disable-line consistent-return
  try {
    const response = await axios("/api/token", {
      method: "post"
    });
    const { data } = response;
    return dispatch(receiveCredentials(data));
  } catch (e) {
    //@todo error handling, This can also be offloaded to the server side.
  }
};


/**
 * Should Fetch Credentials, Check Store first if we have data already.
 *
 * @param {string}    token       Token
 * @param {string}    product     Product
 * @param {string}    user        User
 * @param {string}    expires     Expires
 * @returns {boolean}   Function
 */
const shouldFetchCredentials = ({app: { token, product, user, expires }}) => {
  /**
   * @todo, We can check if the token is still valid here, e.g check if
   * Token has expired.
   */
  return token.length === 0 && product.length === 0 && user.length === 0 && expires.length === 0;


};

/**
 * Fetch Credentials If Needed
 * @returns {Function} Function
 */
export const fetchCredentialsIfNeeded = () => async (dispatch, getState) => {
  if (shouldFetchCredentials(getState())) {
    return dispatch(fetchCredentials());
  }
  return getState();
};
