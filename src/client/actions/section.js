import axios from "axios";

const receiveSection = payload => ({
  type: "SECTION_SET",
  payload
});

/**
 * Fetch RealTime data from API
 * @param {string}      token     Token
 * @param {string}      user      User
 * @param {string}      product   Product
 * @returns {Function}  Function
 */
const fetchSection = ({token, user, product}) =>
  async (dispatch) => { //eslint-disable-line consistent-return
    try {
      dispatch(receiveSection({
        isLoading: true
      }));

      const response = await axios({
        url: "/api/section",
        method: "post",
        data: {
          token,
          user,
          product
        }
      });
      const { data } = response;
      return dispatch(receiveSection(data));
    } catch (e) {
      //@todo error handling, This can also be offloaded to the server side.
    } finally {
      dispatch(receiveSection({
        isLoading: false
      }));
    }
  };

export const fetchSectionIfNeeded = ({ token, product, user, section}) => (dispatch) =>
  (dispatch(fetchSection({ token, product, user, section})));
