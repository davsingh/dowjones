import axios from "axios";

/**
 * Receive Tag
 * @param  {object}       payload         API Response from server
 * @returns {{type: string, payload: *}}  Object
 */
const receiveTag = payload => ({
  type: "TAG_SET",
  payload
});

/**
 * Fetch RealTime data from API
 * @param {string}      token     Token
 * @param {string}      user      User
 * @param {string}      product   Product
 * @param {string}      tag       Tag
 * @returns {Function}  Function
 */
const fetchTag = ({token, user, product, tag}) =>
  async (dispatch) => { //eslint-disable-line consistent-return
  try {
    dispatch(receiveTag({
      isLoading: true
    }));

    const response = await axios({
      url: "/api/tag",
      method: "post",
      data: {
        token,
        user,
        product,
        tag
      }
    });
    const { data } = response;
    return dispatch(receiveTag(data));
  } catch (e) {
    //@todo error handling, This can also be offloaded to the server side.
  } finally {
    dispatch(receiveTag({
      isLoading: false
    }));
  }
};

/**
 * Fetch RealTime If Needed
 * @param {string}      token     Token
 * @param {string}      user      User
 * @param {string}      product   Product
 * @param {string}      tag       TaG
 * @returns {Function}  Function
 */
export const fetchTagIfNeeded = ({ token, user, product, tag}) => (dispatch) => {
  return dispatch(fetchTag({ token, user, product, tag}));
};
