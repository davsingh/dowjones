import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { fetchCredentialsIfNeeded } from "../actions/app";
import { fetchTagIfNeeded } from "../actions/tag";
import Tag from "./../components/tag";


export class Container extends Component {

  static propTypes = {
    token: PropTypes.string.isRequired,
    product: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    onFetchCredentialsIfNeeded: PropTypes.func.isRequired,
    onFetchTagIfNeeded: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {
      onFetchCredentialsIfNeeded,
      onFetchTagIfNeeded,
      match: {
        params: {
          tag
        }
      }
    } = this.props;

    onFetchCredentialsIfNeeded().then(() => {
      const {
        token, product, user
      } = this.props;
      onFetchTagIfNeeded({ token, user, product, tag});
    });
  }

  componentDidUpdate(prevProps) {

    if (prevProps.match.params.tag !== this.props.match.params.tag) {
      const {
        onFetchTagIfNeeded,
        token, product, user,
        match: {
          params: {
            tag
          }
        }
      } = this.props;

      onFetchTagIfNeeded({ token, user, product, tag});
    }
  }

  render() {
    return (
      <Fragment>
        <Tag {...this.props}/>
      </Fragment>
    );
  }
}

const mapStateToProps = ({app: { token, user, product }, tag: { articles, isLoading }}) => ({
  token,
  user,
  product,
  articles,
  isLoading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onFetchCredentialsIfNeeded: fetchCredentialsIfNeeded,
      onFetchTagIfNeeded: fetchTagIfNeeded
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Container);
