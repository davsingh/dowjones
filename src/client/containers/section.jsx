import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Section from "../components/section";
import { fetchCredentialsIfNeeded } from "../actions/app";
import { fetchSectionIfNeeded} from "../actions/section";


export class Container extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    product: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    onFetchCredentialsIfNeeded: PropTypes.func.isRequired,
    onFetchSectionIfNeeded: PropTypes.func.isRequired,
    match: PropTypes.object.isRequired
  };

  componentDidMount() {
    const {
      onFetchCredentialsIfNeeded,
      onFetchSectionIfNeeded,
      match: {
        path
      }
    } = this.props;


    onFetchCredentialsIfNeeded().then(() => {
      const {
        token, product, user
      } = this.props;

      onFetchSectionIfNeeded({ token, product, user, search: path.slice(1)});
    });
  }

  render() {
    return (
      <Fragment>
        <Section {...this.props}/>
      </Fragment>
    );
  }

}

const mapStateToProps = ({app: { token, product, user }, section: { articles, isLoading }}) => ({
  token,
  product,
  user,
  articles,
  isLoading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onFetchCredentialsIfNeeded: fetchCredentialsIfNeeded,
      onFetchSectionIfNeeded: fetchSectionIfNeeded
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Container);
