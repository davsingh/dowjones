import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import Search from "../components/search";
import { fetchSearchIfNeeded } from "../actions/search";
import { fetchCredentialsIfNeeded } from "../actions/app";

export class Container extends Component {

  static propTypes = {
    onFetchCredentialsIfNeeded: PropTypes.func.isRequired
  };

  componentDidMount() {
    const {
      onFetchCredentialsIfNeeded
    } = this.props;

    onFetchCredentialsIfNeeded();
  }

  render() {
    return (
      <Fragment>
        <Search {...this.props}/>
      </Fragment>
    );
  }

}

const mapStateToProps = ({app: { token, user, product }}) => ({
  token,
  user,
  product
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onFetchCredentialsIfNeeded: fetchCredentialsIfNeeded,
      onFetchSearchIfNeeded: fetchSearchIfNeeded
    },
    dispatch
  );


export default connect(mapStateToProps, mapDispatchToProps)(Container);
