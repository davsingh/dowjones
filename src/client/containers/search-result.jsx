import { connect } from "react-redux";
import SearchResult from "../components/search-result";

const mapStateToProps = ({search: { articles, isLoading, query: { name } }}) => ({
  articles,
  isLoading,
  name
});

export default connect(mapStateToProps, null)(SearchResult);
