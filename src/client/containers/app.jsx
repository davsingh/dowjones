import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Section from "../components/section";
import { fetchCredentialsIfNeeded } from "../actions/app";
import { fetchRealTimeIfNeeded } from "../actions/realtime";


export class Container extends Component {
  static propTypes = {
    token: PropTypes.string.isRequired,
    product: PropTypes.string.isRequired,
    user: PropTypes.string.isRequired,
    onFetchCredentialsIfNeeded: PropTypes.func.isRequired,
    onFetchRealTimeIfNeeded: PropTypes.func.isRequired
  };

  componentDidMount() {
    const {
      onFetchCredentialsIfNeeded,
      onFetchRealTimeIfNeeded
    } = this.props;

    onFetchCredentialsIfNeeded().then(() => {
      const {
        token, product, user
      } = this.props;

      onFetchRealTimeIfNeeded({ token, product, user});
    });
  }

  render() {
    return (
      <Fragment>
        <Section {...this.props}/>
      </Fragment>
    );
  }

}

const mapStateToProps = ({app: { token, product, user }, articles: { articles, isLoading }}) => ({
  token,
  product,
  user,
  articles,
  isLoading
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      onFetchCredentialsIfNeeded: fetchCredentialsIfNeeded,
      onFetchRealTimeIfNeeded: fetchRealTimeIfNeeded
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Container);
