import reducer from "../../client/reducers";
import { getToken } from "../plugins/api";
import logger from "electrode-archetype-react-app/lib/logger";

const initTop = async () => {
  try {
    const credentials = "api.demo@efn.co.uk:Password1";
    const buffer = new Buffer(credentials);
    const toBase64 = buffer.toString("base64");
    const response = await getToken(toBase64);
    const { data } = response;

    return {
      reducer,
      initialState: {
        app: {
          token: data["api-token"],
          expires: data["expires-in"],
          product: data["api-product"],
          user: data["api-user"]
        }
      }
    };

  } catch (e) {
    logger.error(e.message);
  }
};

export default initTop;

