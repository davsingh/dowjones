import axios from "axios";
import logger from "electrode-archetype-react-app/lib/logger";
import { getToken } from "./api";

/*eslint-env es6*/
const plugin = {};

plugin.register = async (server, options, next) => {
  server.route({
    method: "POST",
    path: "/api/token",
    handler: async (request, reply) => {

      const credentials = "api.demo@efn.co.uk:Password1";
      const buffer = new Buffer(credentials);
      const toBase64 = buffer.toString("base64");

      try {
        const response = await getToken(toBase64);
        const { data } = response;
        return reply(data);
      } catch (e) {
        logger.error(e.message);
        return reply();
      }
    }
  });

  server.route({
    method: "POST",
    path: "/api/realtime",
    handler: async (request, reply) => {

      const {
        payload: {
          token, user, product
        }
      } = request;


      try {
        const response = await axios({
          url: "https://api.fnlondon.com/realtime",
          method: "get",
          headers: {
            "Api-Token": token,
            "Api-User": user,
            "Api-Product": product,
            Accept: "application/json,version=4.0.0"
          }
        });

        const { data } = response;
        return reply(data);
      } catch (e) {
        logger.error(e.message);
        return reply();
      }
    }
  });


  server.route({
    method: "POST",
    path: "/api/tag",
    handler: async (request, reply) => {

      const {
        payload: {
          token, user, product, tag
        }
      } = request;

      try {
        const response = await axios({
          url: `https://api.fnlondon.com/tag/${tag}`,
          method: "get",
          headers: {
            "Api-Token": token,
            "Api-User": user,
            "Api-Product": product,
            Accept: "application/json,version=4.0.0"
          }
        });

        const { data } = response;
        return reply(data);
      } catch (e) {
        logger.error(e.message);
        return reply();
      }
    }
  });


  server.route({
    method: "POST",
    path: "/api/search",
    handler: async (request, reply) => {

      const {
        payload: {
          token, user, product, search
        }
      } = request;

      try {
        const response = await axios({
          url: `https://api.fnlondon.com/search/${search}`,
          method: "get",
          headers: {
            "Api-Token": token,
            "Api-User": user,
            "Api-Product": product,
            Accept: "application/json,version=4.0.0"
          }
        });

        const { data } = response;
        return reply(data);
      } catch (e) {
        logger.error(e.message);
        return reply();
      }
    }
  });


  server.route({
    method: "POST",
    path: "/api/section",
    handler: async (request, reply) => {

      const {
        payload: {
          token, user, product, section
        }
      } = request;

      try {
        const response = await axios({
          url: `https://api.fnlondon.com/search/${section}`,
          method: "get",
          headers: {
            "Api-Token": token,
            "Api-User": user,
            "Api-Product": product,
            Accept: "application/json,version=4.0.0"
          }
        });

        const { data } = response;
        return reply(data);
      } catch (e) {
        logger.error("/api/section", e.message, token, user, product, section);
        return reply();
      }
    }
  });


  next();
};

plugin.register.attributes = {
  name: "Api",
  version: "0.0.1"
};

module.exports = plugin;
