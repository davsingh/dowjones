import axios from "axios/index";

export const getToken = (credentials) => axios("https://api.fnlondon.com/token", {
  method: "post",
  headers: {
    Authorization: `Basic ${credentials}`,
    Accept: "application/json,version=4.0.0"
  }
});
